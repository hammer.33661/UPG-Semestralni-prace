package visualisation;

import org.knowm.xchart.XChartPanel;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYChartBuilder;
import org.knowm.xchart.XYSeries;
import water_network.Pipe;
import water_network.Reservoir;

import javax.swing.*;
import java.awt.*;


public class ChartWindow extends JFrame {

    static final String WINDOW_TITLE_PREFIX = "UPG SP // Graph";
    static final Dimension DEFAULT_DIMENSION = new Dimension(600, 480);

    Chartable target;
    String name;
    JPanel chartPanel;
    Timer drawTimer;

    public ChartWindow(Chartable target, String name) {
        // attributes
        this.target = target;
        this.name = name;

        // decide chart target type
        boolean isReservoir = target instanceof ReservoirDraw;
        boolean isPipe = target instanceof PipeDraw;

        // chart style
        XYChart chart;
        styling: {
            String yAxisName = "";
            if (isReservoir) yAxisName = "Reservoir Content [m\u00B3]";
            if (isPipe) yAxisName = "Pipe flow [m\u00B3/s]";

            chart = new XYChartBuilder()
                    .xAxisTitle("Elapsed simulation time [s]").yAxisTitle(yAxisName)
                    .width(DEFAULT_DIMENSION.width).height(DEFAULT_DIMENSION.height)
                    .build();

            chart.getStyler()
                    .setDefaultSeriesRenderStyle(XYSeries.XYSeriesRenderStyle.Line);

            chart.getStyler()
                    .setLegendVisible(false)
                    .setChartFontColor(Styles.MAIN_OUTLINE)
                    .setChartBackgroundColor(Styles.RESERVOIR_WATER)
                    .setPlotBackgroundColor(Styles.COMPONENT_BACKGROUND)
                    .setPlotBorderColor(new Color(0,0,0,0));

            chart.getStyler()
                    .setMarkerSize(5)    // debug size of points
                    // .setMarkerSize(0)       // don't paint points
                    .setAxisTickMarksColor(Styles.MAIN_OUTLINE)
                    .setAxisTickLabelsColor(Styles.MAIN_OUTLINE)
                    .setPlotGridLinesColor(Styles.RESERVOIR_BACKGROUND)
                    .setSeriesColors(new Color[] {Styles.RESERVOIR_WATER});
        }

        // init data series
        if (isReservoir) {
            Reservoir r = ((ReservoirDraw) target).RESERVOIR;
            chart.getStyler().setYAxisMin(0.0).setYAxisMax(r.capacity);
        } else if (isPipe) {
            Pipe p = ((PipeDraw) target).PIPE;
            // style.setYAxisMin(0.0);  // values can be negative soo... meh.
        }
        chart.addSeries(name, target.getValues()[0], target.getValues()[1]);

        // add panel
        XChartPanel<XYChart> chartpanel = new XChartPanel(chart);
        this.chartPanel = chartpanel;
        chartpanel.setDoubleBuffered(true);
        chartPanel.setPreferredSize(DEFAULT_DIMENSION);
        this.add(chartPanel);

        // Update values
        drawTimer = new Timer(16, (e) -> {
            chart.updateXYSeries(name, target.getValues()[0], target.getValues()[1], null);
            chartPanel.repaint();
        });
        drawTimer.start();

        // window prefs
        setTitle(WINDOW_TITLE_PREFIX + "~" + name);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);   // Bind close action
        setPreferredSize(DEFAULT_DIMENSION);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    @Override
    public void dispose() {
        super.dispose();
        drawTimer.stop();  // stop drawing timer on window dispose
    }
}