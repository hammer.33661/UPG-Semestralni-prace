package visualisation;

import water_network.NetworkNode;
import water_network.Pipe;
import water_network.Reservoir;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.geom.*;
import java.util.ArrayList;

public interface DrawnObject {
    /**
     * Draw a graphical representation onto the graphics component. <br/>
     *
     * The component drawn uses the set transformation to figure out the position,
     * then draws the component without the scaling, i.e. whatever the transformation
     * of passed graphics context is, the scaling doesn't affect the size
     * of the component, only the {@code glyphSize} does. <br/>
     *
     * @param g  A graphics context to draw the represenation of this object onto
     */
    void drawOnto(Graphics2D g);

    /**
     * Returns a shape that is translated in transform and then scaled back down,
     * with the intention to not change the scaling of the object. <br/>
     * In other words: <br/>
     *   Shape gets translated in the transform and then the scale is reset.
     *
     * @param shape     Shape to get translated
     * @param transform Transform to translate the shape in
     * @param offset    How much the shape should get translated in {@code transform}
     * @return  Translated {@code shape}
     */
    static Shape getTranslatedShape(Shape shape, AffineTransform transform, Point2D offset) {

        if (transform == null) transform = new AffineTransform();

        // save a copy of input transform, ie. don't change parameter
        AffineTransform t = new AffineTransform();

        // do a translation in scale
        t.translate(offset.getX(), offset.getY());
        // reset transform's scale
        t.scale(1/transform.getScaleX(), 1/transform.getScaleY());

        // return a transformed shape
        return t.createTransformedShape(shape);
    }
}

interface Clickable {
    /**
     * Checks if the clickable object is being hit by a point at the passed position.
     * The transform passed is needed to get the graphics context's transformation
     * and get the transformed representation of the object.
     *
     * @param point A hit-tested point (for ex. a mouse pointer)
     * @param tf    Transformation of graphics context
     * @return      Has the representation been hit by the passed point?
     */
    boolean isHit(Point2D point, AffineTransform tf);

    /**
     * Executes the object's <b>on-click</b> action.
     * @param e  The mouse event object from fired click event
     */
    void action(MouseEvent e);

    /**
     * Executes the object's <b>on-scroll</b> action.
     * @param e  The mouse wheel event object from fired scroll event
     */
    void action(MouseWheelEvent e);
}

interface Chartable {
    /**
     * Adds a value (type depends on implementation) to list of values used
     * in object's chart. The {@code simtime} parameter specifies the x-value
     * in the chart - the simulation time when the value was pulled from
     * the water network object (see {@code WaterNetwork.currentSimulationTime()}). <br/>
     *
     * The simulation time should also ensure that x-values (time) is always
     * increasing and values in the list are sorted ascending by x-values.
     *
     * @param simtime  Simulation time of pulling the object's value
     */
    void addCurrentValueAt(float simtime);

    /**
     * Getter for the list of charting values. The values are represented by a two-item array.
     * The 0 index is considered the x-coordinate list (simulation time),
     * the 1 index is the y-coordinate list (charted value). <br/>
     *
     * The list <i>should</i> be sorted, ascending by the x-values.
     *
     * @return  List of charting values.
     */
    ArrayList<Float>[] getValues();

    /**
     * A helper function to get the smallest difference of any two angles.
     * @param a An angle (in degrees)
     * @param b An angle (in degrees)
     * @return  The smallest difference of two angles
     */
    static double angleDifference(double a, double b) {
        // Normalize angles to <0,360>
        a = a % 360;
        if (a < 0) a = 360 + a;
        b = b % 360;
        if (b < 0) b = 360 + b;

        double diff = Math.abs(a - b);
        return (diff > 180) ? (360-diff) : (diff);
    }

    /**
     * Reduces the charting values of a list by the following criteria:
     * <ul>
     *     <li>
     *         If two lines are close enough to being perpendicular,
     *         remove their mid-point.
     *     </li>
     *     <li>
     *         If two points are close enough and their connecting
     *         line and the previous line are close enough to being
     *         perpendicular, remove the mid-point.
     *     </li>
     * </ul>
     * @param data A list of value pairs, must be sorted by x (index 0)
     * @return
     */
    static ArrayList<Float>[] reduce(ArrayList<Float>[] data) {

        long start = System.currentTimeMillis();

        ArrayList<Float> xdata = data[0], ydata = data[1];
        int n = xdata.size();
        if (n < 10) return data;
        float dx = 0f;
        float datarange = xdata.get(n-1) - xdata.get(0);

        // Removal criteria thresholds (pretty much magic numbers)
        double PROXIMITY_FACTOR = 0.005;       // factor of xdata value range
        double PROXIMITY_REMOVAL_ANGLE = 5.0;  // in degrees
        double SAME_ANGLE = 0.005;             // in degrees

        for (int i=1; i<n-1; i++) {
            dx += xdata.get(i) - xdata.get(i-1);

            if (i < 5 || i > n-5) continue;  // skip removal of a few starting and ending points

            // Reduction criteria
            double ang1 = Math.toDegrees(Math.atan2(ydata.get(i) - ydata.get(i-1), xdata.get(i) - xdata.get(i-1)));
            double ang2 = Math.toDegrees(Math.atan2(ydata.get(i+1) - ydata.get(i), xdata.get(i+1) - xdata.get(i)));
            double angleDiff = angleDifference(ang1, ang2);
            boolean anglesAreAlmostSame  = angleDiff < SAME_ANGLE;
            boolean xValuesAreTooClose   = dx < datarange * PROXIMITY_FACTOR;
            boolean anglesAreCloseEnough = angleDiff < PROXIMITY_REMOVAL_ANGLE;

            // Reduce based on criteria
            if (anglesAreAlmostSame || (xValuesAreTooClose && anglesAreCloseEnough)) {
                xdata.remove(i);
                ydata.remove(i);
                n--; i--;  // update pointer on removal
            } else {
                dx = 0f;
            }
        }

        long time = System.currentTimeMillis() - start;
        if (time >= 5) System.out.println(time + "ms (data reduction took a bit too long)");
        return data;
    }
}

class NodeDraw implements DrawnObject {

    /** The default size of the node. */
    static final int DEFAULT_SIZE = 10;

    /** An instance of the {@code WaterNetwork}'s {@code NetworkNode} object being represented. */
    NetworkNode NODE;

    /** A size of the node, hopefully specified by the glyph size of the visualisation */
    int size;


    /**
     * The {@code NodeDraw(NetworkNode node, int size)} with the parameter
     * set to {@code DEFAULT_SIZE}.
     * @param node  Visualised node instance
     */
    public NodeDraw(NetworkNode node) {
        this(node, DEFAULT_SIZE);
    }

    /**
     * The constructor initialises a new {@code NodeDraw} instance, with the passed
     * parameters as its fields. <br/>
     * The node is represented by a small circle, with the {@code size} field as
     * the circle's diameter.
     *
     * @param node  Visualised node instance
     * @param size  Size of the node (diameter)
     */
    public NodeDraw(NetworkNode node, int size) {
        this.NODE = node;
        this.size = size;
    }

    /**
     * Creates a basic shape used for drawing this instance's representation.
     * It is positioned with the center of the shape at the (0,0) coordinate.
     * @return  A centered basic shape of this representation
     */
    private Shape getShape() {
        double halfsize = size / 2.0;
        Ellipse2D node = new Ellipse2D.Double(
                -halfsize, -halfsize,
                size, size);
        return node;
    }

    @Override
    public void drawOnto(Graphics2D g) {

        // Move shape to position, without changing scale
        Shape node = DrawnObject.getTranslatedShape(
                this.getShape(), g.getTransform(), NODE.position );

        g.setColor(Styles.RESERVOIR_WATER);
        g.fill(node);
    }
}

/**
 * The class represents a {@code Reservoir} with the ability to draw
 * itself to a graphics context.
 */
class ReservoirDraw implements DrawnObject, Clickable, Chartable {

    /** An instance of the {@code WaterNetwork}'s {@code NetworkNode} object being represented. */
    Reservoir RESERVOIR;

    /** Graphing values, see {@code Chartable.getValues()}. */
    ArrayList<Float>[] values = new ArrayList[2];

    /** Sizing factor for ensuring glyph size and proper scaling, see {@code getSize()}. */
    double sizingFactor;

    /** A saved last drawn object used for checking hits. */
    Shape lastDrawnBox;

    /**
     * Constructor of the drawn reservoir. <br/>
     *
     * The reservoir is represented by a square. The side of the square is determined
     * by the {@code getSize()} method. The representation is made up by two parts:
     * <ul>
     *     <li>
     *         The background square, representing the shell of the reservoir.
     *     </li>
     *     <li>
     *         The water present in the reservoir, with the water-level being as high
     *         as the reservoir's {@code capacity} is filled with its {@code content}.
     *         If the {@code capacity} is half-full, the water-level will be halfway
     *         up the reservoir.
     *     </li>
     * </ul> <br/>
     *
     * The reservoir also initialises its {@code sizingFactor} to 1 and initialises
     * the lists for charting values.
     *
     * @param r Instance of a {@code Reservoir} object from {@code WaterNetwork}.
     */
    public ReservoirDraw(Reservoir r) {
        this.RESERVOIR = r;
        this.sizingFactor = 1.0;
        this.values[0] = new ArrayList<>();
        this.values[1] = new ArrayList<>();
    }

    @Override
    public void addCurrentValueAt(float time) {
        values[0].add(time);
        values[1].add((float)RESERVOIR.content);
        Chartable.reduce(values);
    }
    @Override
    public ArrayList<Float>[] getValues() {
        return values;
    }

    @Override
    public void action(MouseEvent e) {
        new ChartWindow(this, "Reservoir");
        System.out.println("Opening a chart window reservoir content of \n " + this);
    }
    @Override
    public void action(MouseWheelEvent e) {
        // Do nothing on scroll above reservoir
    }

    /**
     * Getter of the drawn size. This size is the model size multiplied by the sizing factor.
     * To get the model size, the static {@code getModelSize(double capacity)} method is used.
     * @return model_size * sizing_factor
     */
    public double getSize() {
        return ReservoirDraw.getModelSize(RESERVOIR.capacity) * this.sizingFactor;
    }

    /**
     * Getter for the size in model. It is used to generalize how the {@code capacity} of a reservoir
     * should be used to calculate a size in model; i.e. if the capacity is a volume, the size (length of
     * side in a cube) should be the cube root of the capacity field.
     *
     * @return Size of a square representing the reservoir's model capacity (not counting sizing factor).
     */
    public static double getModelSize(double capacity) {
        return Math.cbrt(capacity);
    }

    /**
     * Setter of the sizing factor. This factor is used to multiply the model size
     * to incorporate glyph size.
     * @param sizingFactor Sizing factor
     */
    public void setSizingFactor(double sizingFactor) {
        this.sizingFactor = sizingFactor;
    }

    /**
     * Getter of the box's shape. Uses {@code getSize()} to get a size of the reservoir (side of square)
     * and then makes a square shape to represent the reservoir. The shape is centered around
     * the (0,0) coordinate.
     *
     * @return Square Rectangle2D shape using {@code getSize()}, centered around the (0,0) coordinate
     */
    private Shape getShape() {
        double size = this.getSize();
        Rectangle2D box = new Rectangle2D.Double(
                -size/2.0,
                -size/2.0,
                size, size);
        return box;
    }

    /**
     * Getter of the water's shape. It is very similiar to the {@code getShape()} method,
     * only the water-level is only as high as content and capacity of the reservoir
     * suggest. The shape is still position in such a way that if layered over the basic
     * shape, these two are still centered and water is on the bottom of the reservoir.
     * @return Rectangle shape, sitting at the bottom of the basic shape
     */
    private Shape getWaterShape() {
        double size = this.getSize();
        double height = size * (RESERVOIR.content / RESERVOIR.capacity);
        Rectangle2D water = new Rectangle2D.Double(
                -size/2.0,
                -size/2.0 + size-height,
                size, height
        );
        return water;
    }

    /**
     * Returns the basic shape, translated to the reservoir's model position,
     * not affected by any transformation. It is mainly used for calculating
     * the model->window transformation.
     * @return  The basic shape, translated to reservoir's model position.
     */
    public Shape getModelPositionedShape() {
        Shape box = DrawnObject.getTranslatedShape(
            this.getShape(), null, RESERVOIR.position);
        return box;
    }

    @Override
    public boolean isHit(Point2D point, AffineTransform tf) {
        if (tf.createTransformedShape(lastDrawnBox).contains(point)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void drawOnto(Graphics2D g) {

        Shape box = DrawnObject.getTranslatedShape(
                this.getShape(), g.getTransform(), RESERVOIR.position);
        Shape water = DrawnObject.getTranslatedShape(
                this.getWaterShape(), g.getTransform(), RESERVOIR.position);

        this.lastDrawnBox = box;

        g.setColor(Styles.RESERVOIR_BACKGROUND);
        g.fill(box);
        g.setColor(Styles.RESERVOIR_WATER);
        g.fill(water);
    }
}


class PipeDraw implements DrawnObject, Clickable, Chartable {

    private static final String CUBIC_METERS_PER_SEC = "m\u00B3/s";

    /** An instance of the {@code WaterNetwork}'s {@cod Pipe} object being represented. */
    final Pipe PIPE;

    /** An instance to this pipe's valve representation. */
    final ValveDraw VALVE;

    /** Graphing values, see {@code Chartable.getValues()}. */
    ArrayList<Float>[] values = new ArrayList[2];

    /** A font for this pipe's flow caption. */
    Font font;

    /** Sizing factor for ensuring glyph size and proper scaling, see {@code getThickness()}. */
    double sizingFactor;

    /** A saved last drawn pipe, used for checking hits. */
    Shape lastDrawnPipe;

    /**
     * Constructor of the drawn pipe. <br/>
     *
     * The pipe is represented by a thick line between two nodes or reservoirs, determined by
     * the {@code Pipe}'s {@code start} and {@code end}. The drawn pipe is made up of two parts,
     * both having their own hitboxes and click/scroll actions:
     * <ul>
     *     <li>
     *         A thick line representing the connection between two nodes/reservoirs. This
     *         line's thickness is scaled by its cross section. The pipe is always blue,
     *         regardless of its flow.
     *     </li>
     *     <li>
     *         A valve in the center of the pipe. The valve displays the current "openness"
     *         of the valve, shown by a pie cut-out section, and the direction of flow
     *         described by the caption around the valve.
     *     </li>
     * </ul> <br/>
     *
     * The pipe automatically creates its own valve and initializes its charting value lists.
     *
     * @param pipe Instance of a {@code Pipe} object from {@code WaterNetwork}.
     * @param font Font used for flow captions of this pipe.
     */
    public PipeDraw(Pipe pipe, Font font) {
        this.PIPE = pipe;
        this.VALVE = new ValveDraw(this);
        this.font = font;
        values[0] = new ArrayList<>();
        values[1] = new ArrayList<>();
    }

    @Override
    public void addCurrentValueAt(float simtime) {
        values[0].add(simtime);
        values[1].add((float)PIPE.flow);
        Chartable.reduce(values);
    }
    @Override
    public ArrayList<Float>[] getValues() {
        return values;
    }

    @Override
    public void action(MouseEvent e) {
        new ChartWindow(this, "Pipe flow");
        System.out.println("Opening a chart window for pipe flow of \n " + this);
    }

    @Override
    public void action(MouseWheelEvent e) {
        // Do nothing on pipe mouse wheeling
    }

    /**
     * Sets the sizing factor for this pipe's thickness.
     * @param sizingFactor  Sizing factor setting for this pipe's thickness
     */
    public void setSizingFactor(double sizingFactor) {
        this.sizingFactor = sizingFactor;
    }

    /**
     * A static method for getting a thickness from the {@code crossSection} attribute of
     * {@code WaterNetwork}'s {@code Pipe}. This way, calculating the thickness
     * of pipes is centralized.
     * @param crossSection  Cross section of pipe (see {@code Pipe})
     * @return  A thickness calculated from the cross section
     */
    public static double getModelThickness(double crossSection) {
        return Math.sqrt(crossSection);
    }

    /**
     * Getter for a thickness calculated by {@code Pipe.getModelThickness(crossSection)} scaled
     * by this pipe's sizing factor.
     * @return  A thickness scaled by the sizing factor
     */
    public double getThickness() {
        return getModelThickness(PIPE.crossSection) * sizingFactor;
    }

    /**
     * Getter for a point in the center of the pipe. This method is used to determine where
     * the valve should be drawn.
     * @return  Center of the pipe
     */
    public Point2D getCenter() {
        return new Point2D.Double(
                (PIPE.end.position.getX() + PIPE.start.position.getX()) * 0.5,
                (PIPE.end.position.getY() + PIPE.start.position.getY()) * 0.5
        );
    }

    @Override
    public boolean isHit(Point2D point, AffineTransform tf) {
        if (lastDrawnPipe == null) return false;

        // Get transformed pipe
        PathIterator i = lastDrawnPipe.getPathIterator(null);
        double[] start = new double[6];
        double[] end = new double[6];
        i.currentSegment(start);
        i.next();
        i.currentSegment(end);
        Line2D pipe = new Line2D.Double(start[0], start[1], end[0], end[1]);

        double distance = pipe.ptSegDist(point);
        double tolerance = Math.max(10, getThickness()/2);
        if (distance < tolerance) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void drawOnto(Graphics2D g) {
        this.drawOnto(g, g);
    }

    /**
     * A overloaded {@code drawOnto(Graphics2D g)} allowing draw to 2 different graphics
     * contexts - one for drawn pipes and their valves, the other for the flow captions.
     * This way it's possible to have captions always on top of all other components,
     * making the captions always visible.
     * @param gp  Graphics context for drawing pipes
     * @param gt  Graphics context for drawing texts and captions
     */
    public void drawOnto(Graphics2D gp, Graphics2D gt) {

        // Pipe drawing
        Stroke pipeStroke = new BasicStroke((int) this.getThickness(), BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
        Shape pipe = new Line2D.Double(PIPE.start.position, PIPE.end.position);

        AffineTransform saved = gp.getTransform();
        {
            gp.setTransform(new AffineTransform());

            gp.setColor(Styles.PIPE_WATER);
            gp.setStroke(pipeStroke);
            lastDrawnPipe = saved.createTransformedShape(pipe);
            gp.draw(lastDrawnPipe);
        }
        gp.setTransform(saved);


        // Valve drawing
        VALVE.drawOnto(gp);


        // Flow text drawing
        if (font != null) {

            double valveSize = VALVE.getSize();
            double posX = getCenter().getX();
            double posY = getCenter().getY();

            double dx = PIPE.end.position.getX() - PIPE.start.position.getX();
            double dy = PIPE.end.position.getY() - PIPE.start.position.getY();
            dx *= gt.getTransform().getScaleX();  // get transformed coordinates
            dy *= gt.getTransform().getScaleY();

            double angle = Math.atan2(dy, dx);
            if (angle > Math.toRadians(90) && angle < Math.toRadians(270))
                angle += Math.PI;  // upside down text fix

            FontMetrics fm = gt.getFontMetrics(font);
            String flowText = String.format("%.2f " + CUBIC_METERS_PER_SEC, PIPE.flow);
            int textWidth = fm.stringWidth(flowText);

            AffineTransform savedT = gt.getTransform();
            {
                // gt.setTransform(new AffineTransform());
                gt.translate(posX, posY);
                gt.scale(1 / savedT.getScaleX(), 1 / savedT.getScaleY());
                gt.rotate(angle);
                gt.translate(-textWidth/2.0, -valveSize/2.0 - 2);

                gt.setColor(Styles.FLOW_FONT_COLOR);
                gt.setFont(font);

                gt.drawString(flowText, 0, 0);

            }
            gt.setTransform(savedT);
        }
    }
}

class ValveDraw implements DrawnObject, Clickable {

    /** A minimal size for the valve's representation (diameter). */
    final static int MIN_SIZE = 20;

    /** An instance of valve's pipe representation. */
    final PipeDraw PIPEDRAW;

    /** A saved last drawn pipe */
    Shape lastDrawnValve;


    /**
     * Constructor of the drawn valve. <br/>
     *
     * The valve displays the current "openness" of the valve, shown by a pie-shaped
     * cut-out section, and the direction of flow described by the caption
     * around the valve.
     *
     * @param pipe Instance of a parenting {@code PipeDraw} that this valve belongs to.
     */
    public ValveDraw(PipeDraw pipe) {
        this.PIPEDRAW = pipe;
    }

    @Override
    public void action(MouseEvent e) {
        // Do nothing on valve click
    }
    @Override
    public void action(MouseWheelEvent e) {
        if (e.getWheelRotation() > 0) {
            this.openBy(+0.1);
        } else if (e.getWheelRotation() < 0) {
            this.openBy(-0.1);
        }
    }


    /**
     * Opens the valve by a fraction (percentage if you will). Positive value
     * opens the valve, a negative value closes it.
     * @param amount  A value between <-1,+1>
     */
    public void openBy(double amount) {
        Pipe p = this.PIPEDRAW.PIPE;
        p.open += amount;
        // ensure <0-1> value
        p.open = Math.max(0.0, p.open);
        p.open = Math.min(1.0, p.open);
    }

    /**
     * Getter for the valve's size (diameter), depending on this valve's pipe's thickness.
     * The valve's size is limited to {@code MIN_SIZE}.
     * @return The calculated size (diameter) of this valve.
     */
    public double getSize() {
        return Math.max(PIPEDRAW.getThickness() * 1.2, MIN_SIZE);
    }

    /**
     * Getter of the valve's shape. Uses {@code getSize()} to get a diameter and makes a circle
     * to represent the valve. The shape is centered around the (0,0) coordinate.
     *
     * @return Circle Ellipse2D shape using {@code getSize()}, centered around the (0,0) coordinate
     */
    public Shape getShape() {
        double size = this.getSize();
        Ellipse2D valve = new Ellipse2D.Double(
                -size/2, -size/2,
                size, size);
        return valve;
    }

    /**
     * Getter of the valve's pie-cut to represent the water flowing through the valve.
     * The shape is positioned inside the {@code getShape()}'s shape.
     *
     * @return Pie-shaped Arc2D shape, positioned as overlap for {@code getShape()}.
     */
    public Shape getFlowShape() {
        double size = this.getSize();
        int MAX_ANGLE = 360;
        int START_ANGLE = 270;
        int flowAngle = (int) (PIPEDRAW.PIPE.open * MAX_ANGLE);
        Arc2D flowInValve = new Arc2D.Double(
                -size/2, -size/2,
                size, size,
                START_ANGLE - flowAngle/2.0,
                flowAngle,
                Arc2D.PIE);
        return flowInValve;
    }

    /**
     * Getter for the valve's arrow pointing from pipe's start to end. This arrow is
     * positioned as an overlap of {@code getShape()}. The shape gets rotated around
     * the (0,0) coordinate (meaning around the main shape's center).
     *
     * @param angle  Angle by which the arrow has to rotate to point from start to end of pipe.
     * @return       A rotated arrow shape to be overlapped <i>under</i> {@code getShape()}.
     */
    public Shape getArrowRotatedShape(double angle) {
        double size = this.getSize();
        Path2D arrow = new Path2D.Double();
        arrow.moveTo(0, +size/2);
        arrow.lineTo(size*0.9, 0);
        arrow.lineTo(0, -size/2);
        arrow.closePath();

        AffineTransform rotation = new AffineTransform();
        rotation.rotate(angle);

        return arrow.createTransformedShape(rotation);
    }

    @Override
    public boolean isHit(Point2D point, AffineTransform tf) {
        if (tf.createTransformedShape(lastDrawnValve).contains(point)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void drawOnto(Graphics2D g) {

        Pipe pipe = PIPEDRAW.PIPE;
        double dx = pipe.end.position.getX() - pipe.start.position.getX();
        double dy = pipe.end.position.getY() - pipe.start.position.getY();
        dx *= g.getTransform().getScaleX();
        dy *= g.getTransform().getScaleY();
        double angle = Math.atan2(dy, dx);

        Shape valvePositioned = DrawnObject.getTranslatedShape(
                this.getShape(), g.getTransform(),
                PIPEDRAW.getCenter());
        Shape flowPositioned = DrawnObject.getTranslatedShape(
                this.getFlowShape(), g.getTransform(),
                PIPEDRAW.getCenter());
        Shape arrowPositioned = DrawnObject.getTranslatedShape(
                this.getArrowRotatedShape(angle), g.getTransform(),
                PIPEDRAW.getCenter());

        lastDrawnValve = valvePositioned;

        // Save transform
        AffineTransform saved = g.getTransform();

        // Arrow
        g.setColor(Styles.VALVE_WATER);
        g.fill(arrowPositioned);

        // Arrow outline
        g.setTransform(new AffineTransform());
        {
            g.setColor(Styles.VALVE_OUTLINE);
            g.setStroke(Styles.ARROW_STROKE);
            g.draw(saved.createTransformedShape(arrowPositioned));
        }
        g.setTransform(saved);

        // Valve background
        g.setColor(Styles.VALVE_BACKGROUND);
        g.fill(valvePositioned);

        // Valve flow
        g.setColor(Styles.VALVE_WATER);
        g.fill(flowPositioned);

        // Valve outline
        g.setTransform(new AffineTransform());
        {
            g.setStroke(Styles.VALVE_STROKE);
            g.setColor(Styles.VALVE_OUTLINE);
            g.draw(saved.createTransformedShape(valvePositioned));
        }
        g.setTransform(saved);
        // TODO outlines are sometimes stretched!
    }
}