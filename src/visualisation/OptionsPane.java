package visualisation;

import water_network.Pipe;
import water_network.Reservoir;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * <p>
 *     An options panel with components for controlling the {@code WaterNetworkDrawing}
 *     that it's connected with. The panel also displays details of the currently hovered
 *     object in the visualisation and how to control the visualisation and its objects.
 * </p><p>
 *     The user can use this panel to change glyph size, reset the zoom/pan or export
 *     the visualisation to PNG and SVG formats.
 * </p>
 *
 * @author Milan Kladivko (A17B0245P)
 */
public class OptionsPane extends JPanel {

    // Fonts
    private static final String FONT_FAMILY = "Helvetica";
    private static final Font
            MAIN_FONT = new Font(FONT_FAMILY, Font.PLAIN, 16),
            SMALLER_FONT = new Font(FONT_FAMILY, Font.PLAIN, 12),
            TINY_FONT = new Font(FONT_FAMILY, Font.PLAIN, 10),
            BUTTON_FONT = new Font(FONT_FAMILY, Font.PLAIN, 12);

    // Paddings
    private static final EmptyBorder
            SECTION_PADDING = new EmptyBorder(12,0,0,0),
            SUBTEXT_INDENT = new EmptyBorder(0, 6, 0, 0);


    /** The controlled target {@code WaterNetworkDrawing} visualisation object instance */
    WaterNetworkDrawing target;
    // Option panel's sub-panels
    Box p_glyphSize, p_zoomPanReset, p_simulationSpeed, p_selectedObject, p_export;
    /** An instance to a currently hovered-over visualisation object,
     *  which will have its details displayed in the {@code p_selectedObject} panel */
    DrawnObject selected;

    /** Selected object's detail panel update rate (in milliseconds)  */
    private static final int DETAILS_UPDATE_RATE = 100;
    /** Timer for updating the selected object detail panel ({@code p_selectedObject}) */
    Timer selectedObjectRefresh;


    /**
     * A constructor for the {@code OptionsPane} control and detail panel. This panel contains
     * multiple control components affecting the visualisation, buttons for exporting the
     * visualisation to PNG and SVG, and displays details about the currently hovered-over
     * object and options for interacting with this object.
     *
     * @param wnd  A controlled visualisation instance
     */
    public OptionsPane(WaterNetworkDrawing wnd) {
        // Get water network drawing instance
        this.target = wnd;

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        setBorder(new EmptyBorder(8,8,8,8));

        // TODO add and implement a font size slider??

        // Glyph size section
        this.p_glyphSize = Box.createVerticalBox();
        p_glyphSize.setAlignmentX(Component.LEFT_ALIGNMENT);
        p_glyphSize.setBorder(SECTION_PADDING);
        {
            JLabel heading = new JLabel("Glyph Size [px]");
            heading.setFont(MAIN_FONT);
            heading.setAlignmentX(Component.LEFT_ALIGNMENT);
            p_glyphSize.add(heading);

            JLabel subheading = new JLabel("Adjust the size of components");
            subheading.setFont(TINY_FONT);
            p_glyphSize.add(subheading);

            JSlider slider = new JSlider(JSlider.HORIZONTAL, target.MIN_GLYPHSIZE, target.MAX_GLYPHSIZE, target.getGlyphSize());
            slider.setFont(TINY_FONT);
            slider.setAlignmentX(Component.LEFT_ALIGNMENT);
            heading.setLabelFor(slider);

            slider.setPaintTicks(true);
            slider.setPaintLabels(true);
            slider.setSnapToTicks(true);
            slider.setMajorTickSpacing(50);
            slider.setMinorTickSpacing(10);

            slider.addChangeListener(e -> target.setGlyphSize(slider.getValue()));

            p_glyphSize.add(slider);
        }
        add(p_glyphSize);

        // Reset pan and zoom button
        this.p_zoomPanReset = Box.createVerticalBox();
        p_zoomPanReset.setBorder(SECTION_PADDING);
        {
            JButton resetZoomAndPan = new JButton("Reset zoom and pan");
            resetZoomAndPan.setFont(BUTTON_FONT);
            resetZoomAndPan.addActionListener(e -> {
                target.resetZoomPan();
            });
            p_zoomPanReset.add(resetZoomAndPan);
        }
        add(p_zoomPanReset);


        // Simulation speed section
        this.p_simulationSpeed = Box.createVerticalBox();
        p_simulationSpeed.setBorder(SECTION_PADDING);
        {
            JLabel label = new JLabel("Simulation speed");
            label.setFont(MAIN_FONT);
            label.setAlignmentX(Component.LEFT_ALIGNMENT);
            p_simulationSpeed.add(label);

            ButtonGroup group = new ButtonGroup();
            JRadioButton normal = new JRadioButton("Normal speed", true);
            JRadioButton fast = new JRadioButton("Fast speed", false);
            normal.setFont(SMALLER_FONT);
            fast.setFont(SMALLER_FONT);
            p_simulationSpeed.add(normal);
            p_simulationSpeed.add(fast);
            group.add(normal);
            group.add(fast);

            normal.setActionCommand("normal");
            fast.setActionCommand("fast");
            ActionListener speedChange = e -> {
                if (e.getActionCommand().equals("normal")) {
                    target.setNormalSimulationSpeed();
                } else if (e.getActionCommand().equals("fast")) {
                    target.setFastSimulationSpeed();
                }
            };
            normal.addActionListener(speedChange);
            fast.addActionListener(speedChange);
        }
        add(p_simulationSpeed);

        // Export to PNG/SVG section
        this.p_export = Box.createVerticalBox();
        p_export.setBorder(SECTION_PADDING);
        {
            JLabel heading = new JLabel("Export scheme");
            heading.setFont(MAIN_FONT);
            p_export.add(heading);

            Box exportButtons = Box.createVerticalBox();
            {
                JButton toPng = new JButton("to PNG");
                toPng.setFont(BUTTON_FONT);
                toPng.addActionListener(e -> {
                    // TODO Prompt user for image dimensions
                    // Prompt user for filename
                    String filepath = promptUserForSaveDestination();
                    // Call water network to draw itself
                    target.exportToPNG(filepath, target.getSize());
                });
                JButton toSvg = new JButton("to SVG");
                toSvg.setFont(BUTTON_FONT);
                toSvg.addActionListener(e -> {
                    // TODO Prompt for image dimensions
                    String filepath = promptUserForSaveDestination();
                    target.exportToSVG(filepath, target.getSize());
                });
                exportButtons.add(toPng);
                exportButtons.add(toSvg);
            }
            p_export.add(exportButtons);
        }
        add(p_export);

        // Initial selected object section
        this.p_selectedObject = Box.createVerticalBox();
        {
            selectNothing();
        }
        add(p_selectedObject);
    }

    /**
     * Updates the selected object panel to show the passed object's details
     * and controls. If the passed object is {@code null}, the panel displays
     * default interaction controls for the visualisation.
     * @param obj  Selected (hovered-over) object in the visualisation
     */
    public void updateSelectedObject(DrawnObject obj) {
        if (obj == this.selected) return;
        this.selected = obj;

        // Clear the component
        p_selectedObject.removeAll();

        // Clear selection timer
        if (selectedObjectRefresh != null) selectedObjectRefresh.stop();
        selectedObjectRefresh = null;

        if (selected instanceof ReservoirDraw) {

            Reservoir r = ((ReservoirDraw) selected).RESERVOIR;
            selectReservoir(r);

        } else if (selected instanceof PipeDraw) {

            Pipe p = ((PipeDraw) selected).PIPE;
            selectPipe(p);

        } else selectNothing();

        validate();  // Show changes in panel's component composition
    }

    /**
     * Selects nothing as the hovered-over object. This method displays the default
     * text in the panel.
     */
    private void selectNothing() {
        Box mainControls = Box.createVerticalBox();
        mainControls.setBorder(SECTION_PADDING);
        {
            // Heading
            JLabel heading = new JLabel("Controls");
            heading.setFont(MAIN_FONT);
            mainControls.add(heading);

            // Subtext
            JLabel pan = new JLabel("[Drag] Pan the image");
            JLabel zoom = new JLabel("[Scroll] Zoom into the image");
            JLabel hover1 = new JLabel("[Hover] Display details");
            JLabel hover2 = new JLabel("and controls for component");
            addListOfSmallTexts(mainControls, new JLabel[] { pan, zoom, hover1, hover2 });
        }
        p_selectedObject.add(mainControls);

    }

    /**
     * Selects a passed reservoir as a hovered-over object and starts updating
     * the panel with the reservoir's details.
     * @param r  The selected (hovered-over) reservoir
     */
    private void selectReservoir(Reservoir r) {

        Box details = Box.createVerticalBox();
        details.setBorder(SECTION_PADDING);
        {
            // Heading
            JLabel heading = new JLabel("Reservoir details");
            heading.setFont(MAIN_FONT);
            details.add(heading);

            // Subtext
            JLabel ID = new JLabel("ID: " + r.ID);
            JLabel name = new JLabel("Name: Reservoir#" + getIdentifier(selected));
            JLabel capacity = new JLabel(" ");
            JLabel content = new JLabel(" ");
            addListOfSmallTexts(details, new JLabel[] {
                    ID, name, capacity, content
            });

            // Update simulation values in time
            selectedObjectRefresh = new Timer(DETAILS_UPDATE_RATE, e -> {
                String capacityStr = String.format("Capacity [m\u00B3]: %.3f", r.capacity);
                String contentStr = String.format("Content [m\u00B3]: %.3f (%.1f%%)", r.content, (r.content / r.capacity * 100));
                capacity.setText(capacityStr);
                content.setText(contentStr);
            });
            selectedObjectRefresh.setInitialDelay(0);
            selectedObjectRefresh.start();
        }
        p_selectedObject.add(details);

        Box controls = Box.createVerticalBox();
        controls.setBorder(SECTION_PADDING);
        {
            JLabel heading = new JLabel("Reservoir Controls ");
            heading.setFont(MAIN_FONT);
            controls.add(heading);

            JLabel leftClick = new JLabel("[LClick] Open chart of content");
            addListOfSmallTexts(controls, new JLabel[] { leftClick });
        }
        p_selectedObject.add(controls);
    }

    /**
     * Selects a passed pipe as a hovered-over object and starts updating
     * the panel with the pipe's details.
     * @param p  The selected (hovered-over) pipe
     */
    private void selectPipe(Pipe p) {

        Box details = Box.createVerticalBox();
        details.setBorder(SECTION_PADDING);
        {
            // Add heading (always the same)
            JLabel heading = new JLabel("Pipe details");
            heading.setFont(MAIN_FONT);
            details.add(heading);

            JLabel name = new JLabel("Name: Pipe#" + getIdentifier(selected));
            JLabel crossSection = new JLabel("Cross section [m\u00B2]: " + p.crossSection);
            JLabel open = new JLabel(" ");
            JLabel flow = new JLabel(" ");

            addListOfSmallTexts(details, new JLabel[] {
                    name, crossSection, open, flow
            });

            // Update simulation values in time
            selectedObjectRefresh = new Timer(DETAILS_UPDATE_RATE, e -> {
                String openStr = String.format("Valve open to: %.1f%%", (p.open * 100));
                String flowStr = String.format("Flow [m\u00B3/s]: %.3f", p.flow);
                open.setText(openStr);
                flow.setText(flowStr);
            });
            selectedObjectRefresh.setInitialDelay(0);
            selectedObjectRefresh.start();
        }
        p_selectedObject.add(details);

        Box controls = Box.createVerticalBox();
        controls.setBorder(SECTION_PADDING);
        {
            JLabel heading = new JLabel("Pipe controls");
            heading.setFont(MAIN_FONT);
            controls.add(heading);

            JLabel leftClick = new JLabel("[LClick] Open a chart of flow");
            JLabel scroll = new JLabel("[Scroll] Close/open valve");

            addListOfSmallTexts(controls, new JLabel[] {
                    leftClick, scroll
            });
        }
        p_selectedObject.add(controls);
    }

    /**
     * Returns a generic identifier that is short and distinguishable enough for the user
     * to use for differentiating the different objects with.
     * @param o  Target for identifying
     * @return   Text identifying the passed object
     */
    private static String getIdentifier(DrawnObject o) {
        String str = o.toString();
        String id = str.split("@")[1];
        return id;
    }

    /**
     * A helper method for adding an array of labels in the same way to avoid code repetition.
     * @param target Labels' destination component
     * @param texts  An array of labels to be added
     */
    private static void addListOfSmallTexts(JComponent target, JLabel[] texts) {

        Box subsection = Box.createVerticalBox();
        subsection.setBorder(SUBTEXT_INDENT);

        for (JLabel label : texts) {
            label.setFont(SMALLER_FONT);
            subsection.add(label);
        }

        target.add(subsection);
    }

    /**
     * Displays a generic window that prompts the user for a file destination with a familiar
     * window.
     * @return  An absolute path and name chosen by the user.
     */
    private String promptUserForSaveDestination() {
        String filepath;
        JFileChooser fc = new JFileChooser();

        if (fc.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
            filepath = fc.getSelectedFile().getAbsolutePath();
        } else {
            filepath = "";
        }

        return filepath;
    }
}