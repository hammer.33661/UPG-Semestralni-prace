package visualisation;

// Utilities
import org.jfree.graphics2d.svg.SVGGraphics2D;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

// Water network imports
import water_network.NetworkNode;
import water_network.Pipe;
import water_network.Reservoir;
import water_network.WaterNetwork;

// Graphics imports
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.awt.image.BufferedImage;

/**
 * The {@code WaterNetworkDrawing} class represents a Panel component that takes in
 * an instance of {@code WaterNetwork} and draws its graphical representation into
 * the component so that the water network becomes readable for a human.
 *
 * @version 1.90
 * @author Milan Kladivko (A17B0245P)
 */
public class WaterNetworkDrawing extends JPanel {

    /** Instance of an options side panel component */
    OptionsPane options;

    // ####################################  DEFAULT COMPONENT SIZE PROPERTIES  ########################################

    // Window (design) sizing
    private static final int DESIGN_WIDTH = 800;
    private static final int DESIGN_HEIGHT = 600;
    private static final int WINDOW_OFFSET = 10;
    private static final int DEFAULT_GLYPH_SIZE = 120;
    public static final int  MIN_GLYPHSIZE = 20, MAX_GLYPHSIZE = 320;

    // ###########################################  LAYOUT AND RESIZING  #################################################

    /** Font used by all pipes to display flow */
    private Font flowFont;

    /** Glyph size, affects the size of all simulation objects (not fonts) */
    private int    glyphSize;
    /** Sizing factor for resizing all reservoirs proportional to the biggest one */
    private double reservoirSizingFactor;
    /** Sizing factor for resizing all pipes proportional to the thickest one */
    private double pipeSizingFactor;

    // Model objects to component stretch and translation transform
    /** Transformation for model->window conversion of coordinates */
    private AffineTransform model2WindowTF = new AffineTransform();
    /** Transformation for zooming and panning in the simulation */
    private AffineTransform zoomPanTF = new AffineTransform();
    /** Temporary previous mouse position used in panning controls */
    private Point2D prevMouse = null;

    // Background wipe buffered image cache
    /** A persistent background wipe image, maybe it can boost some FPS? */
    private BufferedImage backgroundWipe;

    // ###########################################  VISUALIZATION FIELDS  ###########################################

    // Interval simulationTimer for updating visualisation in time
    /** Timer for getting changed values from model objects */
    private Timer simulationTimer;
    /** Timer for adding values into the charting data lists */
    private Timer chartValueTimer;

    // Water-network fields
    /** An instance of the currently simulated water network */
    private WaterNetwork waterNetwork;
    private ArrayList<ReservoirDraw> reservoirs;
    private ArrayList<NodeDraw> nodes;
    private ArrayList<PipeDraw> pipes;

    /** Listener for controlling mouse click events */
    private final MouseListener MOUSE_LISTENER = new MouseListener() {

        @Override
        public void mouseClicked(MouseEvent e) {
            Point2D mouse = e.getPoint();

            // Check if reservoirs or pipes were hit by click

            for (ReservoirDraw rdraw : reservoirs) {
                if (rdraw.isHit(mouse, model2WindowTF)) {
                    rdraw.action(e);    // Call reservoir's onclick action
                    return;             // Prevent any other events
                }
            }
            for (PipeDraw pdraw : pipes) {
                if (pdraw.isHit(mouse, model2WindowTF)) {
                    pdraw.action(e);    // Call pipe's onclick action
                    return;             // Prevent any other events
                }
            }
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            // Reset mouse-drag temporary value, force init on next mouse-drag
            prevMouse = null;
        }

        // Unused...
        @Override
        public void mousePressed(MouseEvent e) { return; }
        @Override
        public void mouseEntered(MouseEvent e) { return; }
        @Override
        public void mouseExited(MouseEvent e) { return; }
    };

    /** Listener for mouse-scrolling */
    private final MouseWheelListener MOUSE_WHEEL_LISTENER = new MouseWheelListener() {
        @Override
        public void mouseWheelMoved(MouseWheelEvent e) {

            Point2D mouse = e.getPoint();

            AffineTransform composite = getCompositeTransform();

            // Check if any valve wasn't hit while scrolling
            for (PipeDraw pdraw : pipes) {
                if (pdraw.VALVE.isHit(mouse, composite)) {
                    pdraw.VALVE.action(e);  // Call valve's onscroll action
                    return;                 // Prevent other events
                }
            }

            // If nothing was hit, zoom in/out
            double scaling = (e.getWheelRotation()<0) ? (+1.01) : (+0.99);
            double x = mouse.getX()/2, y = mouse.getY()/2;
            zoomPanTF.translate(+x, +y);
            zoomPanTF.scale(scaling, scaling);
            zoomPanTF.translate(-x*scaling, -y*scaling);
        }
    };

    /** Listener for mouse movement and dragging */
    private final MouseMotionListener MOUSE_MOVE_LISTENER = new MouseMotionListener() {
        @Override
        public void mouseDragged(MouseEvent e) {

            Point2D mouse = e.getPoint();
            if (prevMouse == null) prevMouse = mouse;  // Init prevMouse

            // Pan the visualisation
            double x = (mouse.getX()-prevMouse.getX()) / zoomPanTF.getScaleX();
            double y = (mouse.getY()-prevMouse.getY()) / zoomPanTF.getScaleY();
            zoomPanTF.translate(+x, +y);

            // Update prevMouse for next drag event
            prevMouse = mouse;
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            Point2D mouse = e.getPoint();

            if (options == null) {
                return;
            }

            // TODO add hover effects??

            AffineTransform composite = getCompositeTransform();

            // Update selected object in options pane, if hovering over it
            for (ReservoirDraw rdraw : reservoirs) {
                if (rdraw.isHit(mouse, composite)) {
                    options.updateSelectedObject(rdraw);
                    return;
                }
            }
            for (PipeDraw pdraw : pipes) {
                if (pdraw.isHit(mouse, composite)) {
                    options.updateSelectedObject(pdraw);
                    return;
                }
            }

            // If not hovering over any object, set null selection
            options.updateSelectedObject(null);
        }
    };

    // ###############################################  CONSTRUCTORS  ###############################################

    /**
     * The constructor for a custom graphics component that draws a visualisation of the passed
     * water network. Uses a default glyph size.
     * @param waterNetwork  Instance of a water network to be drawn onto this component
     */
    public WaterNetworkDrawing(WaterNetwork waterNetwork) {
        this(waterNetwork, DEFAULT_GLYPH_SIZE);
    }

    /**
     * The constructor for a custom graphics component that draws a visualisation of the passed
     * water network. Uses the passed glyph size.
     * @param waterNetwork        Instance of a water network to be drawn onto this component
     * @param glyphSize Glyph size (size of biggest reservoir)
     */
    public WaterNetworkDrawing(WaterNetwork waterNetwork, int glyphSize) {
        // Get water network
        this.waterNetwork = waterNetwork;

        // Sizing variables
        this.glyphSize = glyphSize;
        this.reservoirSizingFactor = 1.0;
        this.flowFont = new Font("Sans serif", Font.PLAIN, (int) (glyphSize * 0.15));

        // Initiate reservoir and node drawing objects into collections
        this.reservoirs = new ArrayList<>(20);
        this.nodes = new ArrayList<>(50);
        for (NetworkNode node : waterNetwork.getAllNetworkNodes()) {
            if (node instanceof Reservoir) {
                reservoirs.add(new ReservoirDraw((Reservoir) node));
            } else {
                nodes.add(new NodeDraw(node));
            }
        }
        this.pipes = new ArrayList<>(50);
        for (Pipe pipe : waterNetwork.getAllPipes()) {
            pipes.add(new PipeDraw(pipe, flowFont));
        }

        // Set dimension of component to preferred design dimension
        this.setPreferredSize(new Dimension(DESIGN_WIDTH, DESIGN_HEIGHT));

        // Recalculate sizing and positioning
        this.reload();

        // Repaint every 20ms
        this.simulationTimer = new Timer(16, (e) -> {
            this.repaint();

            try {
                this.waterNetwork.updateState();
            } catch (RuntimeException ex) {
                System.out.println("Runtime error occurred in Water-Network (simulation) on state update!");
                // ex.printStackTrace(System.out);
            }
        });

        this.chartValueTimer = new Timer(20, (e) -> {
            for (ReservoirDraw r : reservoirs)
                r.addCurrentValueAt((float)waterNetwork.currentSimulationTime());
            for (PipeDraw p : pipes)
                p.addCurrentValueAt((float)waterNetwork.currentSimulationTime());
        });

        // Start simulationTimer and simulation
        this.simulationTimer.start();
        this.chartValueTimer.start();

        // Listen for mouse events
        this.addMouseListener(MOUSE_LISTENER);
        this.addMouseWheelListener(MOUSE_WHEEL_LISTENER);
        this.addMouseMotionListener(MOUSE_MOVE_LISTENER);

        // Make a wipe image for clearing screen
        this.backgroundWipe = new BufferedImage(1, 1, BufferedImage.TYPE_3BYTE_BGR);
        Graphics2D wipe = this.backgroundWipe.createGraphics();
        wipe.setColor(Styles.COMPONENT_BACKGROUND);
        wipe.fillRect(0,0, 1,1);
    }

    // ############################################  SCALING AND POSITIONING  ############################################

    /**
     * Setter for {@code glyphSize}.
     * The value determines how big the biggest node should be drawn as.
     * All the node's drawings will be then scaled by a model_biggest_size:glyphSize ratio.
     * @param glyphSize Maximum size of the nodes, other will be scaled appropriately.
     */
    public void setGlyphSize(int glyphSize) {
        this.glyphSize = glyphSize;
        flowFont = new Font("Serif", Font.PLAIN, (int) (glyphSize * 0.15));
        this.reload();
    }

    /**
     * Getter for the current glyph size
     * @return Currently set glyph size
     */
    public int getGlyphSize() {
        return this.glyphSize;
    }

    /**
     * Sets the {@code WaterNetwork}'s simulation speed to fast.
     */
    public void setFastSimulationSpeed() { waterNetwork.runFast(); }

    /**
     * Sets the {@code WaterNetwork}'s simulation speed to normal.
     */
    public void setNormalSimulationSpeed() { waterNetwork.runNormal(); }

    /**
     * Sets the visualisation options panel.
     * @param panel  A visualisation options panel
     */
    public void setOptions(OptionsPane panel) { this.options = panel; }

    /**
     * Reloads nodes and pipes from {@code WaterNetwork} and recalculates affected variables.
     */
    public void reload() {
        recalcSizingFactors();
        recalcModelToWindow(getModelInnerBounds(), getModelOuterBounds());
    }

    /**
     * Calculates the sizing factor for size based on {@code glyphSize} and the capacity
     * of the biggest node.
     */
    private void recalcSizingFactors() {

        double maxCapacity = 0;
        for (ReservoirDraw r : reservoirs) {
            double capacity = r.RESERVOIR.capacity;
            maxCapacity = Math.max(maxCapacity, capacity);
        }
        reservoirSizingFactor = (double) glyphSize / ReservoirDraw.getModelSize(maxCapacity);
        reservoirs.forEach(r -> r.setSizingFactor(reservoirSizingFactor));

        double maxCSection = 0;
        for (PipeDraw p : pipes) {
            double cSection = p.PIPE.crossSection;
            maxCSection = Math.max(maxCSection, cSection);
        }
        pipeSizingFactor = (double) glyphSize/4 / PipeDraw.getModelThickness(maxCSection);
        pipes.forEach(p -> p.setSizingFactor(pipeSizingFactor));
    }

    /**
     * Calculates a bounding rectangle of the model's objects to get an outer
     * size of the model - ie. a box that can contain all objects in the model
     * with their size and model's positions.
     *
     * @return Bounding rectangle of all {@code WaterNetwork}'s objects in model.
     */
    private Rectangle2D getModelOuterBounds() {

        boolean nothingYet = true;
        double minX=0, minY=0, maxX=0, maxY=0;

        for (ReservoirDraw reservoirDraw : reservoirs) {

            /* After knowing the biggest reservoir and having a reservoirSizingFactor,
             * use this calculated size to figure out WHERE the reservoirs should be positioned
             */
            Rectangle2D box = reservoirDraw.getModelPositionedShape().getBounds2D();
            // Find max and min values
            if (nothingYet) {
                nothingYet = false;
                minX = box.getMinX();
                minY = box.getMinY();
                maxX = box.getMaxX();
                maxY = box.getMaxY();
            } else {
                minX = Math.min(minX, box.getMinX());
                minY = Math.min(minY, box.getMinY());
                maxX = Math.max(maxX, box.getMaxX());
                maxY = Math.max(maxY, box.getMaxY());
            }
        }

        return new Rectangle2D.Double(minX, minY, maxX-minX, maxY-minY);
    }

    /**
     * Calculates a bounding rectangle of the model's positions (centers)
     * to get inner size of model - ie. max and min positions of nodes
     * in the whole model.
     *
     * @return Maximum and minimum positions of nodes
     */
    private Rectangle2D getModelInnerBounds() {
        boolean nothingYet = true;
        double minX=0, minY=0, maxX=0, maxY=0;

        for (NetworkNode node : waterNetwork.getAllNetworkNodes()) {
            if (nothingYet) {
                nothingYet = false;
                minX = node.position.getX();
                minY = node.position.getY();
                maxX = minX;
                maxY = minY;
            } else {
                minX = Math.min(node.position.getX(), minX);
                minY = Math.min(node.position.getY(), minY);
                maxX = Math.max(node.position.getX(), maxX);
                maxY = Math.max(node.position.getY(), maxY);
            }
        }
        return new Rectangle2D.Double(minX, minY, maxX, maxY);
    }

    /**
     * Recalculates inner transformation properties for transforming coordinates from
     * model to coordinates of window's basis.
     *
     * The calculations are done in such a way that every coordinate that would fit into
     * an outer model bounding box would fit into the window and edges of this outer bounding
     * box would be on the edge of the window (with some offset).
     *
     * @see Point2D     modelToWindowCoords(Point2D coordinants)
     * @see Rectangle2D modelToWindowCoords(Rectangle2D box)
     *
     * @param modelInner Inner bounding box of model (centers of objects)
     * @param modelOuter Outer bounding box of model (centers + sizes of reservoirs etc.)
     */
    private void recalcModelToWindow(Rectangle2D modelInner, Rectangle2D modelOuter) {

        // translation to get positions to edges of window (these 0 positions don't get scaled)
        double modelTrX = -modelInner.getMinX();
        double modelTrY = -modelInner.getMinY();

        double offsetLeft   = modelInner.getMinX() - modelOuter.getMinX() + WINDOW_OFFSET;
        double offsetRight  = modelOuter.getMaxX() - modelInner.getMaxX() + WINDOW_OFFSET;
        double offsetTop    = modelInner.getMinY() - modelOuter.getMinY() + WINDOW_OFFSET;
        double offsetBottom = modelOuter.getMaxY() - modelInner.getMaxY() + WINDOW_OFFSET;

        // Final correcting offset to get scaled model coordinates into window
        double offsetX = offsetLeft;
        double offsetY = offsetTop;

        // Scaling based on model's inner width (don't count with sizes of boxes)
        double modelScX, modelScY;
        if (modelInner.getWidth() > 0) {
            modelScX = (this.getWidth() - offsetLeft - offsetRight) / modelInner.getWidth();
        } else {
            modelScX = 1;
        }

        if (modelInner.getHeight() > 0) {
            modelScY = (this.getHeight() - offsetTop - offsetBottom) / modelInner.getHeight();
        } else {
            modelScY = 1;
        }

        // Save as transform
        this.model2WindowTF = new AffineTransform();
        this.model2WindowTF.translate(modelTrX, modelTrY);
        this.model2WindowTF.scale(modelScX, modelScY);
        this.model2WindowTF.translate(offsetX/modelScX, offsetY/modelScY);


        /* Debug transformation output *
        System.out.println("\nModel -> draw basis transform: ");
        System.out.format(" Input Inner: X(%.0f), Y(%.0f), W(%.0f), H(%.0f) \n",
                modelInner.getX(), modelInner.getY(), modelInner.getWidth(), modelInner.getHeight());
        System.out.format(" Input Outer: X(%.0f), Y(%.0f), W(%.0f), H(%.0f) \n",
                modelOuter.getX(), modelOuter.getY(), modelOuter.getWidth(), modelOuter.getHeight());
        System.out.format(" Output: X(%d), Y(%d), W(%d), H(%d) \n", 0, 0, DESIGN_WIDTH, DESIGN_HEIGHT);
        System.out.format(" Model Scale: X(%.2f) Y(%.2f) \n", modelScX, modelScY);
        System.out.format(" Model Translate: (%.2f , %.2f) \n", modelTrX, modelTrY);
        /**/
    }

    /**
     * Resets the zoom-pan transformation to default (identity matrix).
     */
    public void resetZoomPan() {
        this.zoomPanTF = new AffineTransform();
    }

    /**
     * Creates a concatenated transformation out of the {@code zoomPanTF} and the {@code model2WindowTF},
     * in this order. By using this method, it is guaranteed that zoom-pan and model2window transformations
     * get layered correctly.
     * @return  A concatenated transformation from {@code zoomPanTF} and {@code model2WindowTF}
     */
    public AffineTransform getCompositeTransform() {
        AffineTransform composite = new AffineTransform();
        composite.concatenate(this.zoomPanTF);
        composite.concatenate(this.model2WindowTF);
        return composite;
    }

    // ##########################################  COMPONENT PAINT CALL  ############################################

    @Override
    public void paintComponent(Graphics _g) {
        super.paintComponent(_g);
        Graphics2D g = (Graphics2D) _g;                     // Create a Graphics2D from Graphics


        // Recalculate model to window transform
        recalcModelToWindow(getModelInnerBounds(), getModelOuterBounds());

        // Draw all graphics onto buffer images
        BufferedImage
                nodesBuffer = new BufferedImage(this.getWidth(), this.getHeight(), BufferedImage.TYPE_4BYTE_ABGR),
                pipesBuffer = new BufferedImage(this.getWidth(), this.getHeight(), BufferedImage.TYPE_4BYTE_ABGR),
                textsBuffer = new BufferedImage(this.getWidth(), this.getHeight(), BufferedImage.TYPE_4BYTE_ABGR);
        drawVisualisationToBuffers(this.getWidth(), this.getHeight(), pipesBuffer, nodesBuffer, textsBuffer);

        // Wipe screen
        AffineTransform stretchWipeToComponent = new AffineTransform();
        stretchWipeToComponent.scale(getWidth(), getHeight());
        g.drawImage(this.backgroundWipe, stretchWipeToComponent, null);

        // Draw object layers on top of each other
        g.drawImage(pipesBuffer, 0,0, null);
        g.drawImage(nodesBuffer, 0,0, null);
        g.drawImage(textsBuffer, 0,0, null);
    }

    /**
     * Draws the visualisation <b>onto the image buffers</b>, i.e. the instances get changed by this method.
     * The width and height parameters are used for streching the whole graphics context by a scaling
     * transformation from window to draw the visualisation onto smaller or bigger canvases than
     * the window. <br/>
     *
     * In other words, use width and height if you want to view the same image on bigger canvases,
     * such as exporting to a file. <br/>
     *
     * This implementation allows for custom layering of drawn pipes, nodes and text. <br/>
     *
     * @param width  Width of the drawn visualisation canvas
     * @param height Height of the drawn visualisation canvas
     * @param pipes  An image buffer where pipes will be drawn
     * @param nodes  An image buffer where all nodes (including reservoirs) will be drawn
     * @param texts  An image buffer where all text will be drawn
     */
    public void drawVisualisationToBuffers(int width, int height, BufferedImage pipes, BufferedImage nodes, BufferedImage texts) {

        // Figure out scaling to width/height
        AffineTransform windowToDimension = new AffineTransform();
        windowToDimension.scale((double)width / getWidth(), (double)height / getHeight());

        // Concatenate to window transform
        AffineTransform finalTransform = this.getCompositeTransform();
        finalTransform.concatenate(windowToDimension);

        Graphics2D gn = nodes.createGraphics();
        drawNodesToBuffer: {

            gn.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            gn.setTransform(finalTransform);

            /* *
            Rectangle2D outer = this.getModelOuterBounds();
            gg.setColor(new Color(0, 0, 200, 100));
            gg.fill(outer);
            Rectangle2D inner = this.getModelInnerBounds();
            gg.setColor(new Color(200, 0, 0, 100));
            gg.fill(inner);
            /**/

            for (NodeDraw node : this.nodes) {
                node.drawOnto(gn);
            }
            for (ReservoirDraw reservoir : reservoirs) {
                reservoir.setSizingFactor(reservoirSizingFactor);
                reservoir.drawOnto(gn);
            }
        }

        Graphics2D gp = pipes.createGraphics();
        Graphics2D gt = texts.createGraphics();
        drawPipesAndTextsToBuffers: {

            gp.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            gp.setTransform(finalTransform);

            gt.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            gt.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
            gt.setTransform(finalTransform);

            for (PipeDraw pipe : this.pipes) {
                pipe.drawOnto(gp, gt);
            }
        }

    }


    /**
     * Exports the visualisation to a PNG (bitmap) file with the supplied path and name.
     * Size determines the dimensions of the output image.
     *
     * @param filename  Path and name of the exported file
     * @param size      Size of the exported image
     */
    public void exportToPNG(String filename, Dimension size) {

        // Dimension -> w/h
        int width = (int) size.getWidth();
        int height = (int) size.getHeight();

        // Create a buffer image for export
        BufferedImage export = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR);
        Graphics2D exportGX = export.createGraphics();

        // Draw all graphics onto buffer images
        BufferedImage
                nodesBuffer = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR),
                pipesBuffer = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR),
                textsBuffer = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR);
        drawVisualisationToBuffers(width, height, pipesBuffer, nodesBuffer, textsBuffer);

        // Draw background
        AffineTransform stretchWipeToComponent = new AffineTransform();
        stretchWipeToComponent.scale(getWidth(), getHeight());
        exportGX.drawImage(this.backgroundWipe, stretchWipeToComponent, null);

        // Draw layers
        exportGX.drawImage(pipesBuffer, 0,0, null);
        exportGX.drawImage(nodesBuffer, 0,0, null);
        exportGX.drawImage(textsBuffer, 0,0, null);

        try {
            ImageIO.write(export, "PNG", new File(filename));
            JOptionPane.showConfirmDialog(
                    this.getRootPane(),
                    "Image saved successfully", "Exported to PNG",
                    JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(
                    this.getRootPane(),
                    "Something went wrong when saving the image", "Error when exporting to PNG",
                    JOptionPane.OK_CANCEL_OPTION, JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Exports the visualisation to an SVG (vector graphics) image with the supplied path and name.
     * Size determines the dimensions of the output image.
     *
     * @param filename  Path and name of the exported file
     * @param size      Size of the exported image
     */
    public void exportToSVG(String filename, Dimension size) {

        int width = (int) size.getWidth();
        int height = (int) size.getHeight();

        SVGGraphics2D g = new SVGGraphics2D(width, height);

        // Figure out scaling to width/height
        AffineTransform windowToDimension = new AffineTransform();
        windowToDimension.scale((double)width / getWidth(), (double)height / getHeight());

        // Concatenate to window transform
        AffineTransform finalTransform = new AffineTransform(this.model2WindowTF);
        finalTransform.concatenate(windowToDimension);

        // Set the transform to export canvas
        g.setTransform(finalTransform);

        // Draw components onto the canvas
        for (PipeDraw pipe : pipes)
            pipe.drawOnto(g);
        for (NodeDraw node : nodes)
            node.drawOnto(g);
        for (ReservoirDraw reservoir : reservoirs) {
            reservoir.setSizingFactor(reservoirSizingFactor);
            reservoir.drawOnto(g);
        }

        try {
            String content = g.getSVGElement();
            Files.write(Paths.get(filename), content.getBytes());

            JOptionPane.showConfirmDialog(
                    this.getRootPane(),
                    "Image saved successfully", "Exported to SVG",
                    JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(
                    this.getRootPane(),
                    "Something went wrong when saving the image", "Error when exporting to SVG",
                    JOptionPane.OK_CANCEL_OPTION, JOptionPane.ERROR_MESSAGE);
        }

    }
}
