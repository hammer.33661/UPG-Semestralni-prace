package visualisation;

import java.awt.*;

public interface Styles {

    Stroke RESERVOIR_STROKE = new BasicStroke(1, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER);
    Stroke VALVE_STROKE     = new BasicStroke(1);
    Stroke ARROW_STROKE     = new BasicStroke(1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);

    float WATER_HUE = 0.60f;
    float MAIN_HUE  = WATER_HUE;

    Color COMPONENT_BACKGROUND  = Color.getHSBColor(MAIN_HUE, 0.2f, 0.9f);
    Color MAIN_OUTLINE          = Color.getHSBColor(MAIN_HUE, 0.3f, 0.1f);

    Color RESERVOIR_BACKGROUND  = Color.getHSBColor(MAIN_HUE, 0.2f, 0.3f);
    Color RESERVOIR_WATER       = Color.getHSBColor(WATER_HUE, 0.6f, 0.9f);
    Color RESERVOIR_OUTLINE     = MAIN_OUTLINE;

    Color PIPE_WATER        = Color.getHSBColor(WATER_HUE, 0.6f, 0.6f);
    Color VALVE_OUTLINE     = MAIN_OUTLINE;
    Color VALVE_BACKGROUND  = Color.getHSBColor(MAIN_HUE, 0.5f, 0.2f);
    Color VALVE_WATER       = RESERVOIR_WATER;

    Color FLOW_FONT_COLOR   = MAIN_OUTLINE;
    Color FLOW_ARROW        = MAIN_OUTLINE;

}
