import visualisation.OptionsPane;
import water_network.WaterNetwork;
import visualisation.WaterNetworkDrawing;

import javax.swing.*;
import java.awt.*;

/**
 * Entry point of the application that includes the main method.
 * @author Milan Kladivko (A17B0245P)
 */
public class Main {

    /**
     * Entry point of the application. <br>
     *
     * Parameter 0 is the glyph size of the visualisation. Glyph size is the maximum size
     * of the biggest object in the water network. It is also used as a scalar for
     * other drawn objects such as font size.
     *
     * @param args param[0]: {@code glyphSize} value
     */
    public static void main(String[] args) {

        JFrame f = new JFrame();
        f.setTitle("UPG SP // Water Network");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);   // Bind close action
        // f.setLayout(new BorderLayout());  // layout now done in a splitPane

        int glyphSize = -1;
        if (args.length > 0) {
            try {
                glyphSize = Integer.parseInt(args[0]);
            } catch (Exception e) {
                System.out.println("Glyph size (arg[0]) must be a number >0");
            }
        }

        // Add project visualisation
        int scenario = 4; // test case
        WaterNetwork wn = new WaterNetwork(scenario);
        WaterNetworkDrawing canvas = new WaterNetworkDrawing(wn);
        if (glyphSize>0) canvas.setGlyphSize(glyphSize);
        // f.add(canvas, BorderLayout.CENTER);

        // Add panel with options
        OptionsPane options = new OptionsPane(canvas);
        canvas.setOptions(options);
        // f.add(options, BorderLayout.EAST);

        // Put the canvas and the options into a splitpane to make them resizable in window
        JSplitPane panel = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, canvas, options);
        panel.setDividerSize(3);
        panel.getRightComponent().setMinimumSize(new Dimension(160, Integer.MAX_VALUE));
        f.add(panel);


        f.pack();                                           // Set window size to wnd's design dimension
        f.setMinimumSize(new Dimension(400, 300));          // Set a minimum size of window
        f.setLocationRelativeTo(null);                      // Center
        f.setVisible(true);                                 // Show
    }

}
